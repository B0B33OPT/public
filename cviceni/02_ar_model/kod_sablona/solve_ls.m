function [x] = solve_ls(A, b)
% function [x] = solve_ls(A, b)
%
% implements A\b using QR decomposition.
%
% INPUT: 
% A: an M-by-N matrix. It is assumed that that M>=N and 
% that A has full rank (=N). 
%
% b: an M-by-1 vector
%
% OUTPUT: 
% x: an N-by-1 vector, solving Ax=b in LSQ sense.

% discard the code from here and implement the functionality:
[M, N] = size(A); 
x = randn(N, 1);

